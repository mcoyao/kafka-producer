package com.shipserv.kafka.domain.master;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Brand {

    private Long id;
    private String name;
    private String synonyms;
    private Boolean isDeleted;
}
