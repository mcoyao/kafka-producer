package com.shipserv.kafka.domain.master;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Country {

    private Long id;
    private String name;
    private String code;
    private Boolean isRestricted;
    private Boolean isDeleted;

}
