package com.shipserv.kafka.domain.master;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    private Long id;
    private String name;
    private String synonyms;
    private String displayName;
    private Long parentId;
    private String code;
    private Boolean isDeleted;
}
