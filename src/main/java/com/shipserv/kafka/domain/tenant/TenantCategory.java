package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantCategory {

    private Long id;
    private String name;
    private String synonyms;
    private String displayName;
    private Boolean isPrimary;
    private Tenant tenant;
    private Boolean isDeleted;

}
