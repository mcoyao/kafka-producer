package com.shipserv.kafka.domain.tenant;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tenant {

    private Long id;

    private int type;
    private String name;
    private String description;
    private String tenancyName;

    private Tenant parentTenant;
    private Tenant publicTenant;

    private CompanyDetails companyDetails;
    private Profile profile;

    private ProfileStatus profileStatus;
    private Boolean isActive;
    private Boolean isTestAccount;
    private Boolean isDeleted;

    @JsonPropertyDescription("For processor use only")
    private Boolean processed;

    @JsonPropertyDescription("For processor use only")
    private GroupChange groupChange;

    @JsonPropertyDescription("For processor use only")
    private ProfileStatusChange profileStatusChange;

}
