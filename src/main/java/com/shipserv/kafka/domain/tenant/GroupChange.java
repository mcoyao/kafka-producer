package com.shipserv.kafka.domain.tenant;

import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupChange {

    private ChangeType type;

    @JsonPropertyDescription("Previous parent tenancy name")
    private String previousParent;

    private Boolean processed;

    private Date dataTTL;

    public enum ChangeType {
        PARENT_TO_CHILD,
        CHILD_TO_PARENT,
        PARENT_CHANGE
    }

}
