package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantCatalogueItem {

    private Long id;
    private String description;
    private String explanation;
    private String partNumber;
    private String unitOfMeasure;
    private String pictureFile;
    private String infoFile;
    private String impaNumber;
    private String issaNumber;
    private String manufacturerNumber;
    private String manufacturerName;
    private Double listPrice;
    private String currency;
    private Integer leadTime;
    private String maDocument;
    private Tenant tenant;
    private Boolean isDeleted;

}
