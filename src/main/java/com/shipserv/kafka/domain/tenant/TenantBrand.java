package com.shipserv.kafka.domain.tenant;

import com.shipserv.kafka.domain.master.AuthorisationStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantBrand {

    private Long id;
    private String name;
    private String synonyms;
    private Tenant tenant;
    private AuthorisationStatus partsAuthorisationTypeStatus;
    private AuthorisationStatus serviceAuthorisationTypeStatus;
    private Boolean isDeleted;

}
