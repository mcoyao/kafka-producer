package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Country {

    @Field("id")
    private Long id;
    private String name;
    private String code;
    private Boolean isRestricted;
    private Boolean isDeleted;

}
