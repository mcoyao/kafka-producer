package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    private Long id;
    private String name;
    private String code;
    private Country country;
    private Boolean isObsolete;
    private Boolean isDeleted;
}
