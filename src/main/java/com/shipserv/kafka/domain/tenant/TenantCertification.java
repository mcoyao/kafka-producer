package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantCertification {

    private Long id;
    private Tenant tenant;
    private String certificationNumber;
    private IndustryCertification industryCertification;
    private Date validUntil;
    private Boolean noExpiration;
    private Boolean isDeleted;
    private Boolean processed;

}
