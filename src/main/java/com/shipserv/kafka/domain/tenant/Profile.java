package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile {

    private Long id;
    private String displayName;
    private String homePageUrl;
    private String directoryPublicEmail;
    private OfficeType officeType;
    private Currency currency;
    private Double rating;
    private Double averageRating;
    private Boolean isDeleted;
}
