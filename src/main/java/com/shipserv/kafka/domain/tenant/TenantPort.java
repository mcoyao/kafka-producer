package com.shipserv.kafka.domain.tenant;

import com.shipserv.kafka.domain.master.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TenantPort {

    private Long id;
    private String code;
    private String name;
    private Tenant tenant;
    private Country country;
    private Boolean isRestricted;
    private Boolean isDeleted;

}
