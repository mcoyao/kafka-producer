package com.shipserv.kafka.domain.tenant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OfficeType {

    private Long id;
    private String name;
    private Boolean isDeleted;

}
