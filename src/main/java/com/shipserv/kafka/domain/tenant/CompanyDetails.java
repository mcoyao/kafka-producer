package com.shipserv.kafka.domain.tenant;

import com.shipserv.kafka.domain.master.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDetails {

    private Long id;
    private String descriptionOfBusiness;
    private String tradingEmail;
    private String contactFirstName;
    private String contactLastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String phone1;
    private String phone2;
    private String phoneAfterHours;
    private String fax;
    private Country country;
    private Boolean isDeleted;

}
