package com.shipserv.kafka.domain.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SupplierLineItem {

    private Long tnid;
    private String name;
    private PurchaseOrderLineItem lineItem;
}
