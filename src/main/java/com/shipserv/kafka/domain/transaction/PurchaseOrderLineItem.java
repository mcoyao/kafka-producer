package com.shipserv.kafka.domain.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseOrderLineItem {

    private Date transactionDate;
    private String partNumber;
    private String description;
    private String equipmentManufacturer;
    private String equipmentModel;

}
