package com.shipserv.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.shipserv.kafka.domain.tenant.TenantBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class TenantBrandProducer implements Producer<TenantBrand> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public TenantBrandProducer() {
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
    }

    @Override
    public String send(TenantBrand domain, String event) throws Exception {
        Message<String> message = MessageBuilder
                .withPayload(objectMapper.writeValueAsString(domain))
                .setHeader(KafkaHeaders.TOPIC, "queuing.shipserv.tenant.brand")
                .setHeader("header_event_name", event)
                .build();
        return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
    }

    @Override
    public String send(TenantBrand domain, String event, String dataKey) throws Exception {
        return null;
    }
}
