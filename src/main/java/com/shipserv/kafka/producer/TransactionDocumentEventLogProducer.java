package com.shipserv.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shipserv.common.model.TransactionDocumentEventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TransactionDocumentEventLogProducer implements Producer<TransactionDocumentEventLog> {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public String send(TransactionDocumentEventLog domain, String event) throws Exception {
        Message<String> message = MessageBuilder
                .withPayload(new ObjectMapper().writeValueAsString(domain))
                .setHeader(KafkaHeaders.TOPIC, "queuing.shipserv.transaction_document_event_log")
                .setHeader("header_event_name", event)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();
        return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
    }

    @Override
    public String send(TransactionDocumentEventLog domain, String event, String dataKey) throws Exception {
        return null;
    }
}
