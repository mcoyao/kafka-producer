package com.shipserv.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shipserv.aspnetzerodata.model.UserEditDto;
import com.shipserv.kafka.configuration.PropertyConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class UserProducer implements Producer<UserEditDto> {

    private static Logger logger = LoggerFactory.getLogger(UserProducer.class);

    @Autowired
    private PropertyConfiguration property;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public String send(UserEditDto domain, String event) throws Exception {
        return null;
    }

    @Override
    public String send(UserEditDto domain, String event, String dataKey) throws Exception {
        logger.info("sending document='{}'", domain.toString());

        Message<String> message = MessageBuilder
                .withPayload(new ObjectMapper().writeValueAsString(domain))
                .setHeader(KafkaHeaders.TOPIC, property.getZeroTopic())
                .setHeader("x-custom-header-event", event)
                .setHeader(KafkaHeaders.MESSAGE_KEY, dataKey)
                .build();
        return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
    }
}
