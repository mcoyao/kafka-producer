package com.shipserv.kafka.producer;

import com.shipserv.common.model.transaction.BaseTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shipserv.kafka.configuration.PropertyConfiguration;

import java.util.UUID;

@Component
public class OrderTransactionProducer implements Producer<BaseTransaction> {

	private static Logger logger = LoggerFactory.getLogger(OrderTransactionProducer.class);
	
	@Autowired
	private PropertyConfiguration property;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Override
	public String send(BaseTransaction domain, String event) throws Exception {
		logger.info("sending document='{}'", domain.toString());
		
		 Message<String> message = MessageBuilder
	                .withPayload(new ObjectMapper().writeValueAsString(domain))
	                .setHeader(KafkaHeaders.TOPIC, property.getTransactionTopic())
	                .setHeader("header_event_name", event)
                    .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
	                .build();
		 logger.info("message={}", message);
		return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
	}

	@Override
	public String send(BaseTransaction domain, String event, String dataKey) throws Exception {
		return null;
	}
}
