package com.shipserv.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shipserv.kafka.domain.transaction.SupplierLineItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class SupplierLineItemProducer implements Producer<SupplierLineItem> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public String send(SupplierLineItem domain, String event) {
        try {
            Message<String> message = MessageBuilder
                    .withPayload(objectMapper.writeValueAsString(domain))
                    .setHeader(KafkaHeaders.TOPIC, "queuing.shipserv.transaction_order_item")
                    .build();
            return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String send(SupplierLineItem domain, String event, String dataKey) throws Exception {
        return null;
    }
}
