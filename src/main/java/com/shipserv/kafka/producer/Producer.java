package com.shipserv.kafka.producer;

public interface Producer<T> {
	
	String send(T domain, String event) throws Exception;

	String send(T domain, String event, String dataKey) throws Exception;
	
}
