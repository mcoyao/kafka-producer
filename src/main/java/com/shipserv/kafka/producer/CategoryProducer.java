package com.shipserv.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.shipserv.kafka.domain.master.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class CategoryProducer implements Producer<Category> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public CategoryProducer() {
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
    }

    @Override
    public String send(Category domain, String event) throws Exception {
        Message<String> message = MessageBuilder
                .withPayload(objectMapper.writeValueAsString(domain))
                .setHeader(KafkaHeaders.TOPIC, "queuing.shipserv.category")
                .setHeader("header_event_name", event)
                .build();
        return kafkaTemplate.send(message).isDone() ? "PUBLISHED" : null;
    }

    @Override
    public String send(Category domain, String event, String dataKey) throws Exception {
        return null;
    }
}
