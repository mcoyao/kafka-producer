package com.shipserv.kafka.configuration;

import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SpringFoxConfiguration {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(new ApiInfo(
					"Producer Microservice API", "Producer Microservice API", "v1.0.0",
					null, null, null, null, Lists.newArrayList()))
			.select().apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot"))).build();
	}

}
