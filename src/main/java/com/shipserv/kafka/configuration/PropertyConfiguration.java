package com.shipserv.kafka.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@Getter
public class PropertyConfiguration {

	@Value("${spring.kafka.bootstrap-servers}")
	private String kafkaAddress;
	
	@Value("${kafka.transaction.topic}")
	private String transactionTopic;

	@Value("${kafka.zero.topic}")
	private String zeroTopic;

	@Value("${kafka.notification.topic}")
	private String notificationTopic;
	
}
