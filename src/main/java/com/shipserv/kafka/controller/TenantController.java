package com.shipserv.kafka.controller;

import com.shipserv.kafka.domain.tenant.Tenant;
import com.shipserv.kafka.domain.tenant.TenantBrand;
import com.shipserv.kafka.domain.tenant.TenantCatalogueItem;
import com.shipserv.kafka.domain.tenant.TenantCategory;
import com.shipserv.kafka.domain.tenant.TenantCertification;
import com.shipserv.kafka.domain.tenant.TenantPort;
import com.shipserv.kafka.producer.TenantBrandProducer;
import com.shipserv.kafka.producer.TenantCatalogueItemProducer;
import com.shipserv.kafka.producer.TenantCategoryProducer;
import com.shipserv.kafka.producer.TenantCertificationProducer;
import com.shipserv.kafka.producer.TenantPortProducer;
import com.shipserv.kafka.producer.TenantProducer;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tenant")
public class TenantController {

    private static Logger logger = LoggerFactory.getLogger(TenantController.class);

    @Autowired
    private TenantProducer tenantProducer;

    @Autowired
    private TenantPortProducer tenantPortProducer;

    @Autowired
    private TenantBrandProducer tenantBrandProducer;

    @Autowired
    private TenantCategoryProducer tenantCategoryProducer;

    @Autowired
    private TenantCatalogueItemProducer tenantCatalogueItemProducer;

    @Autowired
    private TenantCertificationProducer tenantCertificationProducer;

    @ApiOperation("send tenant")
    @PostMapping(path = "", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody Tenant tenant, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantProducer.send(tenant, event), HttpStatus.OK);
    }

    @ApiOperation("send tenant port")
    @PostMapping(path = "/port", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantPort tenantPort, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantPortProducer.send(tenantPort, event), HttpStatus.OK);
    }

    @ApiOperation("send tenant category")
    @PostMapping(path = "/category", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantCategory tenantCategory, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantCategoryProducer.send(tenantCategory, event), HttpStatus.OK);
    }

    @ApiOperation("send tenant brand")
    @PostMapping(path = "/brand", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantBrand tenantBrand, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantBrandProducer.send(tenantBrand, event), HttpStatus.OK);
    }

    @ApiOperation("send tenant catalogue item")
    @PostMapping(path = "/catalogue-item", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantCatalogueItem tenantCatalogueItem, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantCatalogueItemProducer.send(tenantCatalogueItem, event), HttpStatus.OK);
    }

    @ApiOperation("send tenant certification")
    @PostMapping(path = "/certification", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantCertification tenantCertification, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(tenantCertificationProducer.send(tenantCertification, event), HttpStatus.OK);
    }
}
