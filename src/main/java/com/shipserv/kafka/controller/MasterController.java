package com.shipserv.kafka.controller;

import com.shipserv.kafka.producer.BrandProducer;
import com.shipserv.kafka.producer.CategoryProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/master")
public class MasterController {

    private static Logger logger = LoggerFactory.getLogger(MasterController.class);

    @Autowired
    private BrandProducer brandProducer;

    @Autowired
    private CategoryProducer categoryProducer;




}
