package com.shipserv.kafka.controller;

import com.shipserv.common.model.TransactionDocumentEventLog;
import com.shipserv.common.model.transaction.BaseTransaction;
import com.shipserv.kafka.producer.TransactionDocumentEventLogProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shipserv.kafka.producer.OrderTransactionProducer;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

	private static Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	@Autowired
	private OrderTransactionProducer producer;

	@Autowired
	private TransactionDocumentEventLogProducer transactionDocumentEventLogProducer;
	
	@PostMapping(path = "", consumes = "application/json")
	public ResponseEntity<String> create(@RequestBody BaseTransaction transaction, @RequestParam String event) throws Exception {
		logger.info("trigger");
		return new ResponseEntity<String>(producer.send(transaction, event), HttpStatus.OK);
	}

	@PostMapping(path = "/event", consumes = "application/json")
	public ResponseEntity<String> create(@RequestBody TransactionDocumentEventLog transaction, @RequestParam String event) throws Exception {
		logger.info("trigger");
		return new ResponseEntity<String>(transactionDocumentEventLogProducer.send(transaction, event), HttpStatus.OK);
	}
}
