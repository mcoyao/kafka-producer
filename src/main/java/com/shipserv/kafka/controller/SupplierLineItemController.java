package com.shipserv.kafka.controller;

import com.shipserv.kafka.domain.transaction.SupplierLineItem;
import com.shipserv.kafka.producer.SupplierLineItemProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/supplier-line-item")
public class SupplierLineItemController {

    private static Logger logger = LoggerFactory.getLogger(SupplierLineItemController.class);

    @Autowired
    private SupplierLineItemProducer supplierLineItemProducer;

    @PostMapping(path = "", consumes = "application/json")
    public ResponseEntity<String> create(@RequestBody List<SupplierLineItem> orders) throws Exception {
        logger.info("trigger");
        orders.forEach(order -> supplierLineItemProducer.send(order, null));
        return new ResponseEntity<String>("DONE", HttpStatus.OK);
    }


}
