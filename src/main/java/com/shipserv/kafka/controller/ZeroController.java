package com.shipserv.kafka.controller;

import com.shipserv.aspnetzerodata.model.CreateOrEditCompanyDetailDto;
import com.shipserv.aspnetzerodata.model.TenantEditDto;
import com.shipserv.aspnetzerodata.model.UserEditDto;
import com.shipserv.kafka.producer.CompanyDetailProducer;
import com.shipserv.kafka.producer.AbpTenantProducer;
import com.shipserv.kafka.producer.UserProducer;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/zero")
public class ZeroController {

    private static Logger logger = LoggerFactory.getLogger(ZeroController.class);

    @Autowired
    private AbpTenantProducer abpTenantProducer;

    @Autowired
    private CompanyDetailProducer companyDetailProducer;

    @Autowired
    private UserProducer userProducer;

    @ApiOperation("send tenant")
    @PostMapping(path = "/tenant", consumes = "application/json")
    public ResponseEntity<String> sendTenant(@RequestBody TenantEditDto tenant, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(abpTenantProducer.send(tenant, event), HttpStatus.OK);
    }

    @ApiOperation("send company details")
    @PostMapping(path = "/company", consumes = "application/json")
    public ResponseEntity<String> sendCompanyDetails(@RequestBody CreateOrEditCompanyDetailDto companyDetail,
                                                     @RequestParam String event, @RequestParam String dataKey) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(companyDetailProducer.send(companyDetail, event, dataKey), HttpStatus.OK);
    }

    @ApiOperation("send user")
    @PostMapping(path = "/user", consumes = "application/json")
    public ResponseEntity<String> sendCompanyDetails(@RequestBody UserEditDto user,
                                                     @RequestParam String event, @RequestParam String dataKey) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(userProducer.send(user, event, dataKey), HttpStatus.OK);
    }

}
