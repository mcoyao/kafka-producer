package com.shipserv.kafka.controller;

import com.shipserv.domain.notification.NotificationDto;
import com.shipserv.kafka.producer.NotificationProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notifications")
public class NotificationController {
    private static Logger logger = LoggerFactory.getLogger(NotificationController.class);

    @Autowired
    private NotificationProducer producer;

    @PostMapping(path = "", consumes = "application/json")
    public ResponseEntity<String> create(@RequestBody NotificationDto notification, @RequestParam String event) throws Exception {
        logger.info("trigger");
        return new ResponseEntity<String>(producer.send(notification, event), HttpStatus.OK);
    }

    /*@PostMapping(path = "/sendgrid/webhook", consumes = "application/json")
    public void testWebhook() {

    }*/

}
