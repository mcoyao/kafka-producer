package com.shipserv.kafka;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.tomcat.util.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;

public class TestMain {

    public static void main(String[] args) throws IOException {

        TestMain tester = new TestMain();
        //tester.displayClassName();
        /*try (ByteArrayOutputStream decryptedStream = new ByteArrayOutputStream();
             FileInputStream input = new FileInputStream("C:\\solutions\\source\\kafka-producer\\src\\main\\resources\\file.txt")) {
            tester.test(input, decryptedStream);
            byte[] additional = "additional".getBytes();
            decryptedStream.close();
            decryptedStream.write(additional, 0, additional.length);
            System.out.println("data: " + new String(decryptedStream.toByteArray()));
        } catch (Exception e) {
            e.printStackTrace();
        }*/

       /* LocalDateTime startOfDay = LocalDateTime.of(OffsetDateTime.now().minusDays(3).toLocalDate(), LocalTime.MIDNIGHT);

        System.out.println(startOfDay.toLocalDate());*/

        File file = new File("C:\\solutions\\source\\kafka-producer\\src\\main\\resources\\file.txt");
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        String base64Data =  new String(encoded, StandardCharsets.UTF_8);
        System.out.println(base64Data);

        System.out.println(URLConnection.guessContentTypeFromStream(
                new ByteArrayInputStream(Base64.decodeBase64(base64Data))));

        /*LocalDateTime endOfDay = LocalDateTime.of(OffsetDateTime.now().toLocalDate(), LocalTime.MAX);
        System.out.println(startOfDay);
        System.out.println(startOfDay.minusDays(3));*/
    }

    private void displayClassName() {
        System.out.println(TestMain.class.getName().equals("com.shipserv.kafka.TestMain"));
    }

    private void test(InputStream input, OutputStream decryptedData) throws IOException {
        int len;
        byte[] buffer = new byte[256];
        while ((len = input.read(buffer)) != -1) {
            decryptedData.write(buffer, 0, len);
        }
        input.close();
        decryptedData.close();
    }


}
